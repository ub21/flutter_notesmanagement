import 'package:flutter/material.dart';
import 'note.dart';

class NotesProvider extends ChangeNotifier {
  List<Note> _notes = [];
  String? _selectedLabel;

  List<Note> get notes => _notes;
  String? get selectedLabel => _selectedLabel;

  void addNote(Note note) {
    _notes.add(note);
    notifyListeners();
  }

  void updateLabel(String? label) {
    _selectedLabel = label;
    notifyListeners();
  }
}
