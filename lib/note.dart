
import 'package:flutter/material.dart';

class Note {
  final int noteId;
  final String noteContent;
  final String noteDate;
  final int userId;
  final String labelId;
  final String updatedAt;
  final String createdAt;

  Note({
    required this.noteId,
    required this.noteContent,
    required this.noteDate,
    required this.userId,
    required this.labelId,
    required this.updatedAt,
    required this.createdAt,
  });

  factory Note.fromJson(Map<String, dynamic> json) {
  return Note(
    noteId: json['note_id'],
    noteContent: json['note_content'],
    noteDate: json['note_date'],
    userId: json['user_id'],
    labelId: json['label_id'],
    updatedAt: json['updated_at'],
    createdAt: json['created_at'],
  );
}


}


