import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'auth_provider.dart';


// import 'login_screen.dart'; // Importing your login.dart file
import 'notes_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ChangeNotifierProvider(
        create: (context) => AuthProvider(), // Provide your AuthProvider
        // child: LoginScreen() // Use LoginScreen as home
        child: NotesScreen(), // Use Notes page as home
      ),
    );
  }
}
