import 'dart:convert';
import 'package:http/http.dart' as http;

Future<void> saveNote() async {
  final url = Uri.parse("http://localhost:4005/api/notes/saveNote");
  final response = await http.post(
    url,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode({
      'note_content': 'Your note content',
      'label_id': 'General',
      'note_date': DateTime.now().toString(),
      'user_id': 1,
    }),
  );

  if (response.statusCode == 200) {
    print('Note saved successfully');
  } else {
    print('Failed to save note');
  }
}

// method for getting notes

Future<void> getNotes(int userId) async {
  final url = Uri.parse('http://localhost:4005/api/notes/getNotes/$userId');
  final response = await http.get(url);

  if (response.statusCode == 200) {
    final data = jsonDecode(response.body);
    // Handle the retrieved notes data
    print('Notes: $data');
  } else {
    print('Failed to fetch notes');
  }
}
