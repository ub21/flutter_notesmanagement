import 'package:flutter/material.dart';

class AuthProvider with ChangeNotifier {
  String _email = '';
  String _password = '';
  bool _isLoggedIn = false;

  String get email => _email;
  String get password => _password;
  bool get isLoggedIn => _isLoggedIn;

  set email(String value) {
    _email = value;
    notifyListeners();
  }

  set password(String value) {
    _password = value;
    notifyListeners();
  }

  void login() {
    // Perform your login logic here
    // For simplicity, let's assume login is successful if email and password are not empty
    if (_email.isNotEmpty && _password.isNotEmpty) {
      _isLoggedIn = true;
    } else {
      _isLoggedIn = false;
    }
    notifyListeners();
  }
}
