import 'package:flutter/material.dart';
import 'add_notesentry.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class NotesScreen extends StatefulWidget {
  @override
  _NotesScreenState createState() => _NotesScreenState();
}

class _NotesScreenState extends State<NotesScreen> {
  List<dynamic> notes = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notes'),
      ),
      body: Column(
        children: [
          Expanded(
            child: FutureBuilder(
              future: getNotes(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  notes = snapshot.data as List<dynamic>;
                  return ListView.builder(
                    itemCount: notes.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(notes[index]['note_content']),
                        subtitle: Text(notes[index]['note_date']),
                      );
                    },
                  );
                }
              },
            ),
          ),
          ElevatedButton(
            onPressed: () {
              // Navigate to add note screen
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddNotes(
                    // Pass the onNoteAdded function to AddNotes
                    onNoteAdded: (note) {
                      setState(() {
                        notes.add(note);
                      });
                    },
                  ),
                ),
              );
            },
            child: Text('Add a Note'),
          ),
        ],
      ),
    );
  }

  // Method for getting all notes with respect to the user ID
  Future<List<dynamic>> getNotes() async {
    final userId = 1;
    final url = Uri.parse("http://192.168.1.9:4005/api/notes/getNotes/$userId");
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body)['notes'];
      return data;
    } else {
      throw Exception('Failed to fetch notes');
    }
  }
}
