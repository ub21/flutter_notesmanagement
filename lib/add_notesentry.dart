import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'note.dart';
import 'package:intl/intl.dart';

class AddNotes extends StatefulWidget {
  final Function(Note) onNoteAdded;

  const AddNotes({Key? key, required this.onNoteAdded}) : super(key: key);

  @override
  _AddNotesState createState() => _AddNotesState();
}

class _AddNotesState extends State<AddNotes> {
  late BuildContext _context;
  String? _selectedLabel;
  TextEditingController _noteContentController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _context = context;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Notes Entry'),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              saveNote(_noteContentController.text, _selectedLabel, 1);
            },
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Text(
              'Today\'s Date & Time:',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              '${DateFormat.yMd().add_Hm().format(DateTime.now())}',
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(height: 20),
            DropdownButtonFormField(
              decoration: InputDecoration(
                labelText: 'Select Label',
                border: OutlineInputBorder(),
              ),
              value: _selectedLabel,
              items: [
                for (var label in labels)
                  DropdownMenuItem(
                    value: label,
                    child: Row(
                      children: [
                        CircleBullet(color: colors[labels.indexOf(label)]),
                        SizedBox(width: 10),
                        Text(label),
                      ],
                    ),
                  ),
              ],
              onChanged: (value) {
                setState(() {
                  _selectedLabel = value as String?;
                });
              },
            ),
            SizedBox(height: 20),
            Text(
              'Note:',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            TextFormField(
              controller: _noteContentController,
              maxLines: 4,
              decoration: InputDecoration(
                hintText: 'Write your note here...',
                border: OutlineInputBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // post api for the backend integration
  Future<void> saveNote(String noteContent, String? labelId, int userId) async {
    final url = Uri.parse("http://192.168.1.9:4005/api/notes/saveNote");
    try {
      final response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          'note_content': noteContent,
          'label_id': labelId,
          'note_date': DateTime.now().toString(),
          'user_id': 1,
        }),
      );
      if (response.statusCode == 200) {
        print('Note saved successfully');
        print('Response body: ${response.body}');
        // print('Response type: ${response.body.runtimeType}');

        final responseData = json.decode(response.body.trim());
        print('Response data : $responseData');
        if (responseData != null && responseData.containsKey('newNote')) {
          final Map<String, dynamic> noteData = responseData['newNote'];
          final Note savedNote = Note.fromJson(noteData);
          widget.onNoteAdded(savedNote);
        } else {
          print('Failed to parse note data from response');
        }
        Navigator.of(_context)
            .pop(); // Navigate back to the previous screen after saving
      } else {
        print('Failed to save note: ${response.reasonPhrase}');
      }
    } catch (e) {
      print('Failed to connect to the server: $e');
    }
  }
}

class LabelBullet extends StatelessWidget {
  final String label;
  final Color color;

  LabelBullet({required this.label, required this.color});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        CircleBullet(color: color),
        SizedBox(width: 5),
        Text(
          label,
          style: TextStyle(fontSize: 16),
        ),
        SizedBox(width: 10),
      ],
    );
  }
}

class CircleBullet extends StatelessWidget {
  final Color color;

  CircleBullet({required this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}

List<String> labels = [
  'General',
  'Allergy',
  'Mood',
  'Emergency',
  'Side Effects'
];
List<Color> colors = [
  Colors.blue,
  Colors.red,
  Colors.green,
  Colors.orange,
  Colors.purple
];
